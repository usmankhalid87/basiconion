﻿namespace eInvoice.Core.Enums
{
    public enum OrderBy
    {
        Ascending,
        Descending
    }
}
