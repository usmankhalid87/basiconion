﻿namespace eInvoice.Core.Constants
{
    public class GlobalConstants
    {
        public const string EmailRegEx = "^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";

        public const string MESSAGE = "Message";

        public const string MESSAGE_TYPE = "MessageType";

        public const int PAGE_SIZE = 10;

        public const int BIG_PAGE_SIZE = 25;

        public const string DEPARTMENT_CODE_ASSEMBLY = "1";

        public const string DEPARTMENT_CODE_MEP = "2";

        public const string DEPARTMENT_CODE_FINISHING = "3";

        public const string SCHEMA_NAME = "DTP";

        public const string PERMISSION_ADMIN = "Admin";

        public const string PERMISSION_SUPERUSER = "SuperUser";

        public const string PERMISSION_PROJECT = "Project";

        public const string PERMISSION_DAILY_LOOK_AHEAD = "Daily Look Ahead";

        public const string PERMISSION_DAILY_LOOK_AHEAD_BAY = "Daily Look Ahead - Bay";

        public const string PERMISSION_DAILY_LOOK_AHEAD_BAY_1 = "Daily Look Ahead - Bay 1";

        public const string PERMISSION_DAILY_LOOK_AHEAD_BAY_2 = "Daily Look Ahead - Bay 2";

        public const string PERMISSION_DAILY_LOOK_AHEAD_BAY_3 = "Daily Look Ahead - Bay 3";

        public const string PERMISSION_DAILY_LOOK_AHEAD_BAY_4 = "Daily Look Ahead - Bay 4";

        public const string PERMISSION_DAILY_LOOK_AHEAD_BAY_5 = "Daily Look Ahead - Bay 5";

        public const string PERMISSION_DAILY_LOOK_AHEAD_BAY_6 = "Daily Look Ahead - Bay 6";

        public const string PERMISSION_DAILY_LOOK_AHEAD_BAY_7 = "Daily Look Ahead - Bay 7";

        public const string PERMISSION_DAILY_LOOK_AHEAD_BAY_8 = "Daily Look Ahead - Bay 8";

        public const string PERMISSION_DAILY_LOOK_AHEAD_TEAM = "Daily Look Ahead - Team ";

        public const string PERMISSION_DAILY_LOOK_AHEAD_BAY_MEP = "Daily Look Ahead - MEP";

        public const string PERMISSION_DAILY_LOOK_AHEAD_BAY_VERIFY = "Daily Look Ahead Bay - Verify";

        public const string PERMISSION_DAILY_LOOK_AHEAD_BAY_ASSEMBLY = "Daily Look Ahead - Assembly";

        public const string PERMISSION_DAILY_LOOK_AHEAD_BAY_FINISHING = "Daily Look Ahead - Finishing";

        public const string PERMISSION_ACTIVITY_COMPLETION_SHEET = "Activity Completion Sheet";

        public const string PERMISSION_REQUIRED_MATERIAL = "Required Material";

        public const string PERMISSION_REQUIRED_MATERIAL_STORE_KEEPER = "Required Material - Store Keeper";

        public const string PERMISSION_MASTERS = "Masters";

        public const string PERMISSION_ALL_REPORTS = "Reports - All";

        public const string PERMISSION_REPORTS_MATERIAL = "Report - Material";

        public const string PERMISSION_REPORTS_PROJECTLIST = "Report - Project List";

        public const string PERMISSION_REPORTS_ACTIVITYLIST = "Report - Activity List";

        public const string PERMISSION_REPORTS_PROFILE_EXCEL = "Report - Profile Excel";

        public const string PERMISSION_REPORTS_BOXACTIVITY = "Report - Box Activity";

        public const string PERMISSION_REPORTS_BOX_ACTIVITY_COMPLETION = "Report - Box Activity Completion";

        public const string PERMISSION_REPORTS_ESTIMATED_RESOURCES_MANPOWER = "Report - Estimated Resources - Manpower";

        public const string PERMISSION_REPORTS_ESTIMATED_RESOURCES_MATERIAL = "Report - Estimated Resources - Material";

        public const string PERMISSION_REPORTS_STOCK = "Report - Stocks";

        public const string PERMISSION_REPORTS_MAPPED_PROFILE = "Report - Mapped Profile for Box";

        public const string PERMISSION_REPORTS_LEAD_LAG = "Report - Lead/Lag Start Date";

        public const string PERMISSION_REPORTS_LABOR_PRODUCTIVITY = "Report - Labor Productivity";

        public const string PERMISSION_REPORTS_PLANNED_VS_ACTUAL = "Report - Planned Vs Actual";

        public const string PERMISSION_REPORTS_CURRENT_SUPERVISOR_ACTIVITIES = "Report - Current Supervisor Activities";

        public const string PERMISSION_REPORTS_CURRENT_BAY_ACTIVITIES = "Report - Current Bay Activities";

        public const string PERMISSION_REPORTS_CURRENT_TEAM_ACTIVITIES = "Report - Current Team Activities";

        public const string PERMISSION_REPORTS_PROJECT_PROGRESS = "Report - Project Progress";

        public const string PERMISSION_YARD = "Yard";

        public const string PERMISSION_VIEW_YARD = "View Yard";

        public const string PERMISSION_STOCK = "Stock";

        public const string PERMISSION_ASSEMBLY_ACS = "Assembly ACS";

        public const string PERMISSION_MEP_ACS = "MEP ACS";

        public const string PERMISSION_FINISHING_ACS = "Finishing ACS";

        public const string PERMISSION_ALL_ACS = "All ACS";

        public const string PERMISSION_CAN_MARK_BOX_AS_COMPLETED = "Can Mark Box As Completed";
    }
}
