﻿using Autofac;
using Autofac.Integration.Mvc;
using eInvoice.Core.Data;
using System.Web.Mvc;
using System.Web.Http;
using eInvoice.Infrastructure.Data.Repository;
using eInvoice.Infrastructure.Data;

namespace eInvoice.Infrastructure.Bootstrapper.Page_Start
{
    public class IocConfig
    {
        public static void RegisterDependencies()
        {
            var builder = new ContainerBuilder();

            // Get your HttpConfiguration.
            var config = GlobalConfiguration.Configuration;

            // Register your MVC controllers. (MvcApplication is the name of
            // the class in Global.asax.)
            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            // OPTIONAL: Register model binders that require DI.
            builder.RegisterModelBinders(typeof(MvcApplication).Assembly);
            builder.RegisterModelBinderProvider();

            // OPTIONAL: Register web abstractions like HttpContextBase.
            builder.RegisterModule<AutofacWebTypesModule>();

            // OPTIONAL: Enable property injection in view pages.
            builder.RegisterSource(new ViewRegistrationSource());

            // OPTIONAL: Enable property injection into action filters.
            builder.RegisterFilterProvider();

            builder.RegisterGeneric(typeof(EntityRepository<>)).As(typeof(IRepository<>)).InstancePerRequest();
            //builder.RegisterGeneric(typeof(BaseService<>)).As(typeof(IService<>)).InstancePerRequest();

            //builder.RegisterType<ActivityService>().As<IActivityService>().InstancePerRequest();


            builder.RegisterType(typeof(UnitOfWork)).As(typeof(IUnitOfWork)).InstancePerRequest();

            //builder.Register<IEntitiesContext>(b =>
            //{
            //    var context = new DuboxContext("DefaultConnection");
            //    return context;
            //}).InstancePerRequest();

            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            //config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }
    }
}
