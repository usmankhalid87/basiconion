﻿using System.Data;

namespace eInvoice.Core.Data
{
    public interface IUnitOfWork
    {
        int SaveChanges();
        IRepository<TEntity> Repository<TEntity>() where TEntity : class;
        void BeginTransaction(IsolationLevel isolationLevel);
        int Commit();
        void Rollback();
        void DetectChanges();
    }
}
