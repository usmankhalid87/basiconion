﻿using eInvoice.Core.Enums;
using eInvoice.Core.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace eInvoice.Core.Data
{
    public interface IRepository<TEntity>
    {
        IQueryable<TEntity> AsQueryable();

        List<TEntity> GetAll();

        List<TEntity> GetAll(Expression<Func<TEntity, bool>> predicate);

        List<TEntity> GetAll(Expression<Func<TEntity, int>> keySelector, OrderBy orderBy = OrderBy.Ascending);

        List<TEntity> GetAll(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includeProperties);

        List<TEntity> GetAllIncluding(Expression<Func<TEntity, int>> keySelector, OrderBy orderBy = OrderBy.Ascending, params Expression<Func<TEntity, object>>[] includeProperties);

        List<TEntity> GetAll(Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, int>> keySelector, OrderBy orderBy = OrderBy.Ascending, params Expression<Func<TEntity, object>>[] includeProperties);

        PaginatedList<TEntity> GetAll(int pageIndex, int pageSize, Expression<Func<TEntity, int>> keySelector, OrderBy orderBy = OrderBy.Ascending);

        PaginatedList<TEntity> GetAll(int pageIndex, int pageSize, Expression<Func<TEntity, int>> keySelector, Expression<Func<TEntity, bool>> predicate, OrderBy orderBy, params Expression<Func<TEntity, object>>[] includeProperties);


        PaginatedList<TEntity> GetAll(int pageIndex, int pageSize, Expression<Func<TEntity, string>> keySelector, Expression<Func<TEntity, bool>> predicate, OrderBy orderBy, params Expression<Func<TEntity, object>>[] includeProperties);


        //PaginatedList<TEntity> GetAll(int pageIndex, int pageSize, string keySelector, Expression<Func<TEntity, bool>> predicate, OrderBy orderBy, params Expression<Func<TEntity, object>>[] includeProperties);


        PaginatedList<TEntity> GetAll(int pageIndex, int pageSize, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> keySelector, Expression<Func<TEntity, bool>> predicate, OrderBy orderBy, params Expression<Func<TEntity, object>>[] includeProperties);

        List<TEntity> GetAllIncluding(params Expression<Func<TEntity, object>>[] includeProperties);

        TEntity GetSingle(Expression<Func<TEntity, bool>> predicate);

        TEntity GetSingleIncluding(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includeProperties);

        List<TEntity> FindBy(Expression<Func<TEntity, bool>> predicate);

        void Insert(TEntity entity);

        void Update(TEntity entity);

        void Delete(TEntity entity);

        void DeleteRange(IEnumerable<TEntity> entities);

        void InsertRange(IEnumerable<TEntity> entities);

        void UpdateRange(IEnumerable<TEntity> entities);

        bool Exists(Expression<Func<TEntity, bool>> predicate);
    }
}
