﻿using eInvoice.Core.Data;
using eInvoice.Core.Enums;
using eInvoice.Core.Extensions;
using eInvoice.Core.Paging;
using eInvoice.Infrastructure.Data.Context;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace eInvoice.Infrastructure.Data.Repository
{
    public class EntityRepository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected readonly IEntitiesContext _context;
        protected readonly IDbSet<TEntity> _dbEntitySet;

        public EntityRepository(IEntitiesContext context)
        {
            _context = context;
            _dbEntitySet = _context.Set<TEntity>();
        }

        public IQueryable<TEntity> AsQueryable()
        {
            return _dbEntitySet;
        }

        public List<TEntity> GetAll(Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, int>> keySelector = null,
            OrderBy orderBy = OrderBy.Ascending, params Expression<Func<TEntity, object>>[] includeProperties)
        {
            var entities = IncludeProperties(includeProperties);
            entities = (predicate != null) ? entities.Where(predicate) : entities;

            if (keySelector != null)
            {
                entities = (orderBy == OrderBy.Ascending) ? entities.OrderBy(keySelector) : entities.OrderByDescending(keySelector);
            }

            return entities.ToList();
        }

        public List<TEntity> GetAll(Expression<Func<TEntity, bool>> predicate)
        {
            IQueryable<TEntity> entities = _dbEntitySet;
            entities = (predicate != null) ? entities.Where(predicate) : entities;
            return entities.ToList();
        }

        public List<TEntity> GetAll(Expression<Func<TEntity, bool>> predicate,
            params Expression<Func<TEntity, object>>[] includeProperties)
        {
            var entities = IncludeProperties(includeProperties);
            entities = (predicate != null) ? entities.Where(predicate) : entities;
            return entities.ToList();
        }

        public List<TEntity> GetAll()
        {
            return _dbEntitySet.ToList();
        }

        public List<TEntity> GetAll(Expression<Func<TEntity, int>> keySelector, OrderBy orderBy = OrderBy.Ascending)
        {
            IQueryable<TEntity> entities = _dbEntitySet;
            if (keySelector != null)
            {
                entities = (orderBy == OrderBy.Ascending) ? entities.OrderBy(keySelector) : entities.OrderByDescending(keySelector);
            }

            return entities.ToList();
        }

        public PaginatedList<TEntity> GetAll(int pageIndex, int pageSize, Expression<Func<TEntity, int>> keySelector, OrderBy orderBy = OrderBy.Ascending)
        {
            return GetAll(pageIndex, pageSize, keySelector, null, orderBy);
        }

        public PaginatedList<TEntity> GetAll(int pageIndex, int pageSize, Expression<Func<TEntity, int>> keySelector,
            Expression<Func<TEntity, bool>> predicate, OrderBy orderBy, params Expression<Func<TEntity, object>>[] includeProperties)
        {
            var entities = FilterQuery(keySelector, predicate, orderBy, includeProperties);
            var total = entities.Count();// entities.Count() is different than pageSize
            entities = entities.Paginate(pageIndex, pageSize);
            return entities.ToPaginatedList(pageIndex, pageSize, total);
        }

        public PaginatedList<TEntity> GetAll(int pageIndex, int pageSize, Expression<Func<TEntity, string>> keySelector,
            Expression<Func<TEntity, bool>> predicate, OrderBy orderBy, params Expression<Func<TEntity, object>>[] includeProperties)
        {
            var entities = FilterQuery(keySelector, predicate, orderBy, includeProperties);
            var total = entities.Count();// entities.Count() is different than pageSize
            entities = entities.Paginate(pageIndex, pageSize);
            return entities.ToPaginatedList(pageIndex, pageSize, total);
        }

        public PaginatedList<TEntity> GetAll(int pageIndex, int pageSize, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> keySelector,
            Expression<Func<TEntity, bool>> predicate, OrderBy orderBy, params Expression<Func<TEntity, object>>[] includeProperties)
        {
            var entities = FilterQuery(keySelector, predicate, orderBy, includeProperties);
            var total = entities.Count();// entities.Count() is different than pageSize
            entities = entities.Paginate(pageIndex, pageSize);
            return entities.ToPaginatedList(pageIndex, pageSize, total);
        }

        public List<TEntity> GetAllIncluding(params Expression<Func<TEntity, object>>[] includeProperties)
        {
            var entities = IncludeProperties(includeProperties);
            return entities.ToList();
        }

        public List<TEntity> GetAllIncluding(Expression<Func<TEntity, int>> keySelector, OrderBy orderBy = OrderBy.Ascending,
             params Expression<Func<TEntity, object>>[] includeProperties)
        {
            var entities = IncludeProperties(includeProperties);
            entities = (orderBy == OrderBy.Ascending) ? entities.OrderBy(keySelector) : entities.OrderByDescending(keySelector);
            return entities.ToList();
        }

        public TEntity GetSingle(Expression<Func<TEntity, bool>> predicate)
        {
            return _dbEntitySet.FirstOrDefault(predicate);
        }

        public TEntity GetSingleIncluding(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includeProperties)
        {
            var entities = IncludeProperties(includeProperties);
            return entities.FirstOrDefault(predicate);
        }

        public List<TEntity> FindBy(Expression<Func<TEntity, bool>> predicate)
        {
            return _dbEntitySet.Where(predicate).ToList();
        }

        public void Insert(TEntity entity)
        {
            _context.SetAsAdded(entity);
        }

        public void Update(TEntity entity)
        {            
            _context.SetAsModified(entity);
        }

        public void Delete(TEntity entity)
        {
            _context.SetAsDeleted(entity);
        }

        public void DeleteRange(IEnumerable<TEntity> entities)
        {
            if (entities == null)
                return;

            foreach (var entity in entities.ToList())
                _context.SetAsDeleted(entity);
        }

        public void InsertRange(IEnumerable<TEntity> entities)
        {
            if (entities == null)
                return;

            foreach (var entity in entities.ToList())
                _context.SetAsAdded(entity);
        }

        private void CommitChangesDuringInsertRange(int count, int commitCount)
        {
            if (count % commitCount == 0)
            {
                //_context.DetectChanges();
                _context.SaveChanges();
            }
        }

        public void UpdateRange(IEnumerable<TEntity> entities)
        {
            if (entities == null)
                return;

            foreach (var entity in entities.ToList())
            {
                _context.SetAsModified(entity);
            }
        }

        public bool Exists(Expression<Func<TEntity, bool>> predicate)
        {
            return _dbEntitySet.Any(predicate);
        }

        private IQueryable<TEntity> FilterQuery(Expression<Func<TEntity, int>> keySelector, Expression<Func<TEntity, bool>> predicate, OrderBy orderBy,
            Expression<Func<TEntity, object>>[] includeProperties)
        {
            var entities = IncludeProperties(includeProperties);
            entities = (predicate != null) ? entities.Where(predicate) : entities;

            if (keySelector != null)
            {
                entities = (orderBy == OrderBy.Ascending)
                    ? entities.OrderBy(keySelector)
                    : entities.OrderByDescending(keySelector);
            }

            return entities;
        }

        private IQueryable<TEntity> FilterQuery(Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> keySelector, Expression<Func<TEntity, bool>> predicate,
            OrderBy orderBy,
            Expression<Func<TEntity, object>>[] includeProperties)
        {
            var entities = IncludeProperties(includeProperties);
            entities = (predicate != null) ? entities.Where(predicate) : entities;

            if (keySelector != null)
            {
                entities = keySelector(entities);
            }

            return entities;
        }

        private IQueryable<TEntity> FilterQuery(Expression<Func<TEntity, string>> keySelector, Expression<Func<TEntity, bool>> predicate, OrderBy orderBy,
            Expression<Func<TEntity, object>>[] includeProperties)
        {
            var entities = IncludeProperties(includeProperties);
            entities = (predicate != null) ? entities.Where(predicate) : entities;

            if (keySelector != null)
            {
                entities = (orderBy == OrderBy.Ascending)
                    ? entities.OrderBy(keySelector)
                    : entities.OrderByDescending(keySelector);
            }

            return entities;
        }        

        private IQueryable<TEntity> IncludeProperties(params Expression<Func<TEntity, object>>[] includeProperties)
        {
            IQueryable<TEntity> entities = _dbEntitySet;
            if (includeProperties != null)
            {
                foreach (var includeProperty in includeProperties)
                {
                    entities = entities.Include(includeProperty);
                }
            }

            return entities;
        }
    }
}
