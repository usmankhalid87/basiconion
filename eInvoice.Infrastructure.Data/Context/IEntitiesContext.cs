﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eInvoice.Infrastructure.Data.Context
{
    public interface IEntitiesContext
    {
        IDbSet<TEntity> Set<TEntity>() where TEntity : class;
        void SetAsAdded<TEntity>(TEntity entity) where TEntity : class;
        void SetAsModified<TEntity>(TEntity entity) where TEntity : class;
        void SetAsDeleted<TEntity>(TEntity entity) where TEntity : class;
        int SaveChanges();
        void SetAutoDetectChanges(bool enable);
        void DetectChanges();
        void BeginTransaction(IsolationLevel isolationLevel);
        int Commit();
        void Rollback();
    }
}
