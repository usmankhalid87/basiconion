﻿using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;

namespace eInvoice.Infrastructure.Data.Context
{
    public class EInvoiceContext : DbContext, IEntitiesContext
    {
        private ObjectContext _objectContext;
        private DbTransaction _transaction;

        public EInvoiceContext(DbConnection connection) : base(connection, true)
        {
            Configuration.LazyLoadingEnabled = false;
            Configuration.AutoDetectChangesEnabled = false;
            Configuration.ValidateOnSaveEnabled = false;
        }

        public EInvoiceContext() : base("name=DefaultConnection")
        {
            Configuration.LazyLoadingEnabled = false;
            Configuration.AutoDetectChangesEnabled = false;
            Configuration.ValidateOnSaveEnabled = false;
        }

        public EInvoiceContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
            Configuration.LazyLoadingEnabled = false;
            Configuration.AutoDetectChangesEnabled = false;
            Configuration.ValidateOnSaveEnabled = false;
        }

        public new IDbSet<TEntity> Set<TEntity>() where TEntity : class
        {
            return base.Set<TEntity>();
        }

        public void SetAsAdded<TEntity>(TEntity entity) where TEntity : class
        {
            UpdateEntityState(entity, System.Data.Entity.EntityState.Added);
        }

        public void SetAsModified<TEntity>(TEntity entity) where TEntity : class
        {
            UpdateEntityState(entity, System.Data.Entity.EntityState.Modified);
        }

        public void SetAsDeleted<TEntity>(TEntity entity) where TEntity : class
        {
            UpdateEntityState(entity, System.Data.Entity.EntityState.Deleted);
        }

        private DbEntityEntry GetDbEntityEntrySafely<TEntity>(TEntity entity) where TEntity : class
        {
            var dbEntityEntry = Entry<TEntity>(entity);
            if (dbEntityEntry.State == System.Data.Entity.EntityState.Detached)
            {
                //Set<TEntity>().Attach(entity);
            }
            return dbEntityEntry;
        }

        private void UpdateEntityState<TEntity>(TEntity entity, System.Data.Entity.EntityState entityState) where TEntity : class
        {
            var dbEntityEntry = GetDbEntityEntrySafely(entity);
            dbEntityEntry.State = entityState;
        }

        public void SetAutoDetectChanges(bool enable)
        {
            Configuration.AutoDetectChangesEnabled = enable;
            Configuration.ValidateOnSaveEnabled = enable;
        }

        public void BeginTransaction(IsolationLevel isolationLevel)
        {
            _objectContext = ((IObjectContextAdapter)this).ObjectContext;
            if (_objectContext.Connection.State == ConnectionState.Open)
            {
                return;
            }
            _objectContext.Connection.Open();
            _transaction = _objectContext.Connection.BeginTransaction(isolationLevel);
        }

        public int Commit()
        {
            var saveChanges = SaveChanges();
            _transaction.Commit();
            return saveChanges;
        }

        public void DetectChanges()
        {
            ChangeTracker.DetectChanges();
        }

        public void Rollback()
        {
            _transaction.Rollback();
        }
    }
}
