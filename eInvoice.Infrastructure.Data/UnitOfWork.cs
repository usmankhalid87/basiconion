﻿using eInvoice.Core.Data;
using eInvoice.Infrastructure.Data.Context;
using eInvoice.Infrastructure.Data.Repository;
using System;
using System.Collections;
using System.Data;

namespace eInvoice.Infrastructure.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IEntitiesContext _context;
        private Hashtable _repositories;

        public UnitOfWork(IEntitiesContext context)
        {
            _context = context;
        }

        public int SaveChanges()
        {
            return _context.SaveChanges();
        }

        public IRepository<TEntity> Repository<TEntity>() where TEntity : class
        {
            if (_repositories == null)
            {
                _repositories = new Hashtable();
            }
            var type = typeof(TEntity).Name;
            if (_repositories.ContainsKey(type))
            {
                return (IRepository<TEntity>)_repositories[type];
            }
            var repositoryType = typeof(EntityRepository<>);
            _repositories.Add(type, Activator.CreateInstance(repositoryType.MakeGenericType(typeof(TEntity)), _context));
            return (IRepository<TEntity>)_repositories[type];
        }

        public void BeginTransaction(IsolationLevel isolationLevel)
        {
            _context.BeginTransaction(isolationLevel);
        }

        public void DetectChanges()
        {
            _context.DetectChanges();
        }

        public int Commit()
        {
            _context.DetectChanges();
            var result = _context.Commit();
            return result;
        }

        public void Rollback()
        {
            _context.Rollback();
        }
    }
}
