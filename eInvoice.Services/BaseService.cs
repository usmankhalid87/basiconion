﻿using eInvoice.Core.Data;
using eInvoice.Core.Enums;
using eInvoice.Core.Paging;
using eInvoice.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace eInvoice.Services
{

    public class BaseService<TEntity> : IService<TEntity> where TEntity : class
    {
        public IUnitOfWork UnitOfWork { get; private set; }
        protected readonly IRepository<TEntity> _repository;
        private bool _disposed;

        public BaseService(IUnitOfWork unitOfWork, IRepository<TEntity> repository)
        {
            UnitOfWork = unitOfWork;
            //_repository = UnitOfWork.Repository<TEntity>();
            _repository = repository;
        }

        public List<TEntity> GetAll()
        {
            return _repository.GetAll();
        }

        public List<TEntity> GetAllIncluding(Expression<Func<TEntity, int>> keySelector,
                                             OrderBy orderBy = OrderBy.Ascending,
                                             params Expression<Func<TEntity, object>>[] includeProperties)
        {
            return _repository.GetAllIncluding(keySelector, orderBy, includeProperties);
        }

        public List<TEntity> GetAllIncluding(params Expression<Func<TEntity, object>>[] includeProperties)
        {
            return _repository.GetAllIncluding(includeProperties);
        }

        public List<TEntity> GetAll(Expression<Func<TEntity, int>> keySelector, OrderBy orderBy = OrderBy.Ascending)
        {
            return _repository.GetAll(keySelector, orderBy);
        }

        public List<TEntity> GetAll(Expression<Func<TEntity, bool>> predicate)
        {
            return _repository.GetAll(predicate);
        }
        public List<TEntity> GetAll(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includeProperties)
        {
            return _repository.GetAll(predicate, includeProperties);
        }

        public List<TEntity> GetAll(Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, int>> keySelector,
            OrderBy orderBy = OrderBy.Ascending, params Expression<Func<TEntity, object>>[] includeProperties)
        {
            return _repository.GetAll(predicate, keySelector, orderBy, includeProperties);
        }

        public PaginatedList<TEntity> GetAll(int pageIndex, int pageSize, Expression<Func<TEntity, int>> keySelector, OrderBy orderBy)
        {
            return _repository.GetAll(pageIndex, pageSize, keySelector, orderBy);
        }

        public PaginatedList<TEntity> GetAll(int pageIndex, int pageSize, Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, int>> keySelector,
            OrderBy orderBy, params Expression<Func<TEntity, object>>[] includeProperties)
        {
            return _repository.GetAll(pageIndex, pageSize, keySelector, predicate, orderBy, includeProperties);
        }

        public PaginatedList<TEntity> GetAll(int pageIndex, int pageSize, Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, string>> keySelector,
            OrderBy orderBy, params Expression<Func<TEntity, object>>[] includeProperties)
        {
            return _repository.GetAll(pageIndex, pageSize, keySelector, predicate, orderBy, includeProperties);
        }

        public PaginatedList<TEntity> GetAll(int pageIndex, int pageSize, Expression<Func<TEntity, bool>> predicate, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> keySelector,
            OrderBy orderBy, params Expression<Func<TEntity, object>>[] includeProperties)
        {
            return _repository.GetAll(pageIndex, pageSize, keySelector, predicate, orderBy, includeProperties);
        }

        public TEntity GetById(Expression<Func<TEntity, bool>> predicate)
        {
            return _repository.GetSingle(predicate);
        }

        public TEntity GetById(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includeProperties)
        {
            return _repository.GetSingleIncluding(predicate, includeProperties);
        }

        public void Add(TEntity entity)
        {
            _repository.Insert(entity);
            UnitOfWork.SaveChanges();
        }

        public void AddRange(IEnumerable<TEntity> entities)
        {
            var insertDateTime = DateTime.Now;
            foreach (var entity in entities)
            {
                _repository.Insert(entity);
            }

            UnitOfWork.SaveChanges();
        }

        public void Update(TEntity entity)
        {
            UnitOfWork.DetectChanges();            
            _repository.Update(entity);
            UnitOfWork.SaveChanges();
        }

        public void UpdateRange(IEnumerable<TEntity> entities)
        {
            foreach (var entity in entities)
            {                
                _repository.Update(entity);
            }

            UnitOfWork.SaveChanges();
        }

        public int Delete(TEntity entity)
        {
            _repository.Delete(entity);
            return UnitOfWork.SaveChanges();
        }

        public int DeleteRange(IEnumerable<TEntity> entities)
        {
            _repository.DeleteRange(entities);
            return UnitOfWork.SaveChanges();
        }
    }
}
