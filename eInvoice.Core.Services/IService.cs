﻿using eInvoice.Core.Data;
using eInvoice.Core.Enums;
using eInvoice.Core.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace eInvoice.Core.Services
{
    public interface IService<TEntity> where TEntity : class
    {
        IUnitOfWork UnitOfWork { get; }

        List<TEntity> GetAll();
        List<TEntity> GetAllIncluding(params Expression<Func<TEntity, object>>[] includeProperties);
        List<TEntity> GetAllIncluding(Expression<Func<TEntity, int>> keySelector,
                                             OrderBy orderBy = OrderBy.Ascending,
                                             params Expression<Func<TEntity, object>>[] includeProperties);

        List<TEntity> GetAll(Expression<Func<TEntity, int>> keySelector, OrderBy orderBy = OrderBy.Ascending);

        List<TEntity> GetAll(Expression<Func<TEntity, bool>> predicate);
        List<TEntity> GetAll(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includeProperties);

        List<TEntity> GetAll(Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, int>> keySelector = null,
            OrderBy orderBy = OrderBy.Ascending, params Expression<Func<TEntity, object>>[] includeProperties);

        PaginatedList<TEntity> GetAll(int pageIndex, int pageSize, Expression<Func<TEntity, int>> keySelector, OrderBy orderBy = OrderBy.Ascending);

        PaginatedList<TEntity> GetAll(int pageIndex, int pageSize, Expression<Func<TEntity, bool>> predicate,
            Expression<Func<TEntity, int>> keySelector, OrderBy orderBy, params Expression<Func<TEntity, object>>[] includeProperties);

        PaginatedList<TEntity> GetAll(int pageIndex, int pageSize, Expression<Func<TEntity, bool>> predicate,
            Expression<Func<TEntity, string>> keySelector, OrderBy orderBy, params Expression<Func<TEntity, object>>[] includeProperties);

        PaginatedList<TEntity> GetAll(int pageIndex, int pageSize, Expression<Func<TEntity, bool>> predicate,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> keySelector, OrderBy orderBy, params Expression<Func<TEntity, object>>[] includeProperties);

        TEntity GetById(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includeProperties);

        TEntity GetById(Expression<Func<TEntity, bool>> predicate);

        void Add(TEntity entity);

        void AddRange(IEnumerable<TEntity> entities);

        void Update(TEntity entity);

        void UpdateRange(IEnumerable<TEntity> entities);

        int Delete(TEntity entity);

        int DeleteRange(IEnumerable<TEntity> entities);
    }
}
